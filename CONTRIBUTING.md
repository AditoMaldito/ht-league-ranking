# Contributing to HT League Ranking

First of all thanks for considering contributing to HT League Ranking. You don't need to be a developper and knowing how to code to help improving this project. We also need some tester and feedbacks from users.

## Feedbacks and test

If you are using HT League Ranking and want to give us feedbacks to enhance HT League Ranking app or if you found a bug you can:

- [Report an issue](https://gitlab.com/LeoMouyna/ht-league-ranking/-/issues/new?) (you have to login into [gitlab](https://gitlab.com) first).
- By sending email to our [help desk](mailto:incoming+leomouyna-ht-league-ranking-18798057-issue-@incoming.gitlab.com)

Once it's done we can work on it and keep you inform about the development.

## Developping

HT League Ranking can be run locally with 2 approches:

- Using docker and docker-compose
- Run each part of the application locally

### Prerequistes

To run HT League Ranking locally you will need some prerequisities:

- Set up an SQL database (which is not part of the dockerfile) with write access user.
- Machine with super user privilege.
- Be a [CHPP developper](https://hattrick.org/goto.ashx?path=/Community/CHPP/ChppBecomingDeveloper.aspx) (developper program for hattrick game). Without it you can help by developping but you won't have access to hattrick data.

### Git Workflow

We use [git](https://git-scm.com/) as version control so you need to install it on your machine.

```sh
sudo apt install git
```

Once git is installed you will have to fork the project then clone it.

```sh
git clone <repository_url>
```

Now let's start developping according to a dedicated issue (for example issue #42):

- You will create a dedicated branch for this issue. `git checkout -b <branch_name>`
- You will do some modification and add at the end of your commits message the issue number. `git commit -m "<your message with a good description>. #42"`
- Once you finsh your developpemnt, you have to open a merge request and wait for CI and review.

That's all we would glad to see and review your first Merge Request.

### Set up database

To set up database we use [alembic](https://alembic.sqlalchemy.org/en/latest/)

Once it's installed you can run:

```sh
cd database
export DATABASE_URL=<database_type>://<user>:<password>@<database_location>:<database_port>
alembic upgrade head
```

Alembic will create and populate needed tables.

### Using docker and docker-compose

HT League Ranking publish docker images on it [own container registery](https://gitlab.com/LeoMouyna/ht-league-ranking/container_registry). But you can also build images locally and run containers via [docker-compose](https://docs.docker.com/compose/)

#### Set up

Install [docker](https://docs.docker.com/get-started/) and [docker compose](https://docs.docker.com/compose/install/).

Update [_.env_](./.env) with your secrets and configuration variables.

#### Run

Once set up is done you just have to run

```sh
docker-compose build
docker-compose up
```

Pay attention to your [_docker-compose.yml_](./docker-compose.yml).

We expose port 80 as default port for frontend access and port 6379 for redis so shutdown any process running on those ports or change your configuration.

Visit HT League Ranking at [`http://localhost`](http://localhost).

### Local runs

If you don't want to use docker images you can run each part of the application locally.

#### Set up

Install all tools needed:

- [python3](https://www.python.org/downloads/)
- [pip](https://pip.pypa.io/en/stable/installing/)
- [redis](https://redis.io/download)
- [nvm](https://github.com/nvm-sh/nvm#installing-and-updating) **recommended** or [npm via node](https://nodejs.org/en/)
- [vuejs](https://vuejs.org/v2/guide/installation.html)

First we recommend to use a virtual env for backend part.

```sh
python3 -m venv ht-league-ranking-back
source ht-league-ranking-back/bin/activate
```

Then you can install all required dependencies:

```sh
cd back-end
pip install -r requirements.txt
```

For front-end part, you need to insall dependencies:

```sh
cd front-end
npm i
```

Export all environment variables needed:

```sh
export HT_ACCESS_TOKEN_KEY=<value>
export HT_ACCESS_TOKEN_SECRET=<value>
export HT_CONSUMER_KEY=<value>
export HT_CONSUMER_SECRET=<value>
export DATABASE_URL=<value>
export REDIS_URL=<value>
export JWT_TOKEN_SECRET=<value>
```

#### Run

You have to run each part of the application locally.

First let's start redis using systemd

```sh
sudo systemctl start redis
```

Then start celery instance:

```sh
cd back-end
celery --app=celery_app worker -l INFO -E
```

Start flask server:

```sh
cd back-end
./start_gunicorn.sh --log-level 'debug'
```

You have to update [_httpService.js_](./front-end/src/services/httpService.js) before start vuejs locally. You can uncomment lines with `localhost:5000` and comment lines with `/api`. But please be carefull to never commit those change, it's only for dev and debug purpose.

Then you can start vuejs app:

```sh
cd front-end
yarn serve
```

Visit HT League ranking at [`http://localhost:8080`](http://localhost:8080)

### Unit Tests

We use [jest](https://jestjs.io/docs/getting-started) for front end unit tests and [pytest](https://docs.pytest.org/en/6.2.x/getting-started.html) for backend unit tests.

> All unit tests will be trigger on each commit by gitlab-ci so make sure they pass on your local env before push your changes.

#### Back-end Unit Tests

You can run all back-end unit tests with:

```sh
cd back-end
pytest -v
```

#### Front-end Unit Tests

You can run all front-end unit tests with:

```sh
cd front-end
nvm use 14
yarn jest
```
