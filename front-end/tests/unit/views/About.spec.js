import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import About from "@/views/About.vue";
import Buefy from "buefy";

config.mocks.$t = key => key;

const localVue = createLocalVue();
localVue.use(Buefy);

let wrapper;
describe("About.vue", () => {
  test("mount correctly", () => {
    wrapper = shallowMount(About, { localVue });
    expect(wrapper).toBeTruthy();
  });
});
