import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import Policy from "@/views/Policy.vue";
import Buefy from "buefy";

config.mocks.$t = key => key;

const localVue = createLocalVue();
localVue.use(Buefy);

let wrapper;
describe("Policy.vue", () => {
  test("mount correctly", () => {
    wrapper = shallowMount(Policy, { localVue });
    expect(wrapper).toBeTruthy();
  });
});
