import Vuex from "vuex";
import ranking from "@/store/modules/ranking";
import division from "@/store/modules/division";
import task from "@/store/modules/task";
import authentication from "@/store/modules/authentication";

export const buildStore = (
  rankingActions,
  rankingState,
  divisionActions,
  divisionState,
  taskActions,
  taskState,
  authenticationActions,
  authenticationState
) => {
  const rAction = rankingActions ?? {
    set: jest.fn(),
    toggleRanking: jest.fn()
  };
  const rState =
    rankingState ??
    (() => ({
      country: "Test Country",
      countryId: 0,
      level: 6,
      firstLeagueId: 0,
      leagueSize: 1
    }));

  const dAction = divisionActions ?? {
    setDivision: jest.fn()
  };
  const dState =
    divisionState ??
    (() => ({
      country: "Test Country",
      countryId: 0,
      level: 6,
      firstLeagueId: 0,
      leagueSize: 1
    }));

  const tAction = taskActions ?? {
    setTask: jest.fn(),
    updateRunningTask: jest.fn(),
    resetTask: jest.fn()
  };
  const tState =
    taskState ??
    (() => ({
      taskId: 0,
      taskStatus: "done",
      country: "Test Country",
      level: 6,
      startDate: new Date().toISOString(),
      ratio: 100
    }));

  const aAction = authenticationActions ?? {
    set: jest.fn(),
    updateToken: jest.fn(),
    updateProfile: jest.fn(),
    logout: jest.fn()
  };

  const aState =
    authenticationState ??
    (() => ({
      serverAuthenticated: false,
      token: undefined,
      profile: undefined
    }));

  const storeOptions = {
    modules: {
      ranking: {
        namespaced: true,
        state: rState,
        actions: rAction,
        getters: ranking.getters
      },
      division: {
        namespaced: true,
        state: dState,
        actions: dAction,
        getters: division.getters
      },
      task: {
        namespaced: true,
        actions: tAction,
        state: tState,
        getters: task.getters
      },
      authentication: {
        namespaced: true,
        actions: aAction,
        state: aState,
        getters: authentication.getters
      }
    }
  };

  return new Vuex.Store(storeOptions);
};
