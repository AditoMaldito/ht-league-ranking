import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import RefreshMessage from "@/components/RefreshMessage.vue";
import Buefy from "buefy";
import Vuex from "vuex";
import ranking from "@/store/modules/ranking";

config.mocks.$t = key => key;
config.mocks.$d = (date, _) => date.toString();

const localVue = createLocalVue();
localVue.use(Buefy);
localVue.use(Vuex);

let state = () => ({
  leagues: [{ name: "test" }],
  last_update: new Date(),
  country: "Test Country",
  level: 6,
  loading: false
});

const store = new Vuex.Store({
  modules: {
    ranking: {
      namespaced: true,
      state,
      getters: ranking.getters
    }
  }
});

let wrapper;
describe("RefreshMessage.vue", () => {
  test("mount correctly", () => {
    wrapper = shallowMount(RefreshMessage, { localVue, store });
    expect(wrapper).toBeTruthy();
  });
});
