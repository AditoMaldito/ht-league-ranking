import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import HeaderForm from "@/components/HeaderForm.vue";
import Buefy from "buefy";
import deviceService from "@/services/deviceService.js";

config.mocks.$t = key => key;

jest.mock("@/services/deviceService.js");

const localVue = createLocalVue();
localVue.use(Buefy);

let wrapper;
describe("HeaderForm.vue", () => {
  test("mount correctly", () => {
    wrapper = shallowMount(HeaderForm, { localVue });
    expect(wrapper).toBeTruthy();
  });
  describe("Select size according to device", () => {
    describe("When device is desktop", () => {
      test("Set size to large", () => {
        expect(deviceService.isMobile).toHaveBeenCalledTimes(1);
        expect(wrapper.vm.$data.size).toBe("is-large");
      });
    });
    describe("When device is a mobile", () => {
      beforeEach(() => {
        deviceService.isMobile
          .mockClear()
          .mockImplementationOnce(() => true)
          .mockName("mobile response");
        wrapper = shallowMount(HeaderForm, { localVue });
      });
      test("Set size to small", () => {
        expect(deviceService.isMobile).toHaveBeenCalledTimes(1);
        expect(wrapper.vm.$data.size).toBe("");
      });
    });
  });
});
