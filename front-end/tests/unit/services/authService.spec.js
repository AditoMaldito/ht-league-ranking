import authService from "@/services/authService";
import Cookies from "js-cookie";
import { API_v1, API_v2 } from "@/services/httpService";

jest.mock("@/services/httpService.js");

describe("authService", () => {
  beforeEach(() => {
    API_v1.get.mockClear();
    API_v2.get.mockClear();
    API_v2.post.mockClear();
  });
  describe("#authStatus", () => {
    test("http call to auth/status", () => {
      authService.authStatus();
      expect(API_v1.get).toHaveBeenCalledTimes(1);
      expect(API_v1.get).toHaveBeenCalledWith("auth/status");
      expect(API_v2.get).toHaveBeenCalledTimes(0);
    });
  });
  describe("#login", () => {
    test("generate a POST request", () => {
      authService.login();
      expect(API_v2.post).toHaveBeenCalledTimes(1);
    });
    test("pass path argument to POST request", () => {
      authService.login();
      expect(API_v2.post).toHaveBeenCalledWith("auth/login", {
        path: expect.any(String)
      });
    });
  });
  describe("#logout", () => {
    test("generate a POST request", () => {
      authService.logout();
      expect(API_v2.post).toHaveBeenCalledTimes(1);
    });
    test("call auth/logout api route", () => {
      authService.logout();
      expect(API_v2.post).toHaveBeenCalledWith("auth/logout", undefined, {
        headers: expect.any(Object)
      });
    });
  });
  describe("#fetchProfile", () => {
    test("generate a GET request", () => {
      authService.fetchProfile();
      expect(API_v2.get).toHaveBeenCalledTimes(1);
    });
    test("call auth/profile api route", () => {
      authService.fetchProfile();
      expect(API_v2.get).toHaveBeenCalledWith("auth/profile", {
        headers: expect.any(Object)
      });
    });
  });
  describe("#findTokenInCookies", () => {
    beforeAll(() => {
      jest.mock("js-cookie", () => jest.fn());
      Cookies.get = jest.fn();
    });
    beforeEach(() => {
      Cookies.get.mockClear();
    });
    test("retrieve token from cookies", () => {
      authService.findTokenInCookies();
      expect(Cookies.get).toHaveBeenCalledWith("TOKEN");
    });
    afterAll(() => {
      jest.unmock("js-cookie");
    });
  });
  describe("#storeToken", () => {
    beforeEach(() => {
      jest.spyOn(localStorage.__proto__, "setItem");
    });
    test("call setItem from localStorage", () => {
      authService.storeToken("MySecretToken");
      expect(localStorage.setItem).toHaveBeenCalledWith(
        "token",
        "MySecretToken"
      );
    });
  });
  describe("#retrieveToken", () => {
    beforeEach(() => {
      jest.spyOn(localStorage.__proto__, "getItem");
    });
    test("call getItem from localStorage", () => {
      authService.retrieveToken();
      expect(localStorage.getItem).toHaveBeenCalledWith("token");
    });
  });
  describe("#removeToken", () => {
    beforeAll(() => {
      jest.mock("js-cookie", () => jest.fn());
      Cookies.remove = jest.fn();
    });
    beforeEach(() => {
      Cookies.remove.mockClear();
      jest.spyOn(localStorage.__proto__, "removeItem");
    });
    test("call removeItem from localStorage", () => {
      authService.removeToken();
      expect(localStorage.removeItem).toHaveBeenCalledWith("token");
    });
    test("call remove from cookies", () => {
      authService.removeToken();
      expect(Cookies.remove).toHaveBeenCalledWith("TOKEN");
    });
    afterAll(() => {
      jest.unmock("js-cookie");
    });
  });
});
