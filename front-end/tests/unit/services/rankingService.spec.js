import rankingService from "@/services/rankingService";
import authService from "@/services/authService";
import { API_v2, API_v1, STREAM_API_V2_URL } from "@/services/httpService";
import EventSource from "eventsource";

jest.mock("@/services/httpService");
jest.mock("@/services/authService");
jest.mock("eventsource");

describe("rankingService", () => {
  describe("API_v1 functions *will be deprecated one day*", () => {
    beforeAll(() => {
      API_v2.get.mockClear();
      API_v2.post.mockClear();
    });
    beforeEach(() => {
      API_v1.get.mockClear();
      API_v1.post.mockClear();
    });
    describe("getOne", () => {
      test("generate a GET request", () => {
        rankingService.getOne(1234);
        expect(API_v1.get).toHaveBeenCalledTimes(1);
      });
      test("use given leagueId on GET url", () => {
        rankingService.getOne(120);
        expect(API_v1.get).toHaveBeenCalledWith("ranking/league/120");

        for (var i = 123; i < 130; i++) {
          const leagueId = i;
          rankingService.getOne(leagueId);
          expect(API_v1.get).toHaveBeenCalledWith(`ranking/league/${leagueId}`);
        }
      });
    });
    describe("download", () => {
      test("generate a POST request", () => {
        rankingService.download([]);
        expect(API_v1.post).toHaveBeenCalledTimes(1);
      });
      test("call export/ranking url", () => {
        rankingService.download([]);
        expect(API_v1.post).toHaveBeenCalledWith(
          "export/ranking",
          expect.any(Object)
        );
      });
      test("use given leagues to generate POST request", () => {
        const leagues = new Array(4).map((_, index) => {
          return { league: `II.${index + 1}`, country: "Sweden" };
        });
        rankingService.download(leagues);
        expect(API_v1.post).toHaveBeenCalledWith("export/ranking", { leagues });
      });
    });
    afterEach(() => {
      expect(API_v2.get).toHaveBeenCalledTimes(0);
      expect(API_v2.post).toHaveBeenCalledTimes(0);
    });
  });
  describe("API_v2 functions", () => {
    beforeAll(() => {
      API_v1.get.mockClear();
      API_v1.post.mockClear();
    });
    beforeEach(() => {
      API_v2.get.mockClear();
      API_v2.post.mockClear();
      authService.buildAuthorizationHeader.mockClear();
    });
    describe("getDivision", () => {
      test("generate a GET request", () => {
        rankingService.getDivision("Sweden", 2);
        expect(API_v2.get).toHaveBeenCalledTimes(1);
      });
      test("call #buildAuthorizationHeader from authService", () => {
        rankingService.getDivision("sweden", 2);
        expect(authService.buildAuthorizationHeader).toHaveBeenCalledTimes(1);
      });
      test("use given arguments to generate GET request", () => {
        rankingService.getDivision("sweden", 2);
        expect(API_v2.get).toHaveBeenCalledWith("divisions/sweden/2", {
          headers: { Authorization: expect.any(String) }
        });
        ["france", "england", "norway"].every((country, index) => {
          rankingService.getDivision(country, index);
          expect(API_v2.get).toHaveBeenCalledWith(
            `divisions/${country}/${index}`,
            {
              headers: { Authorization: expect.any(String) }
            }
          );
        });
      });
      test.skip("use lowercased country name to generate GET request", () => {
        rankingService.getDivision("SweDen", 2);
        expect(API_v2.get).toHaveBeenCalledWith("divisions/sweden/2");
        ["fraNce", "England", "NORWAY"].every((country, index) => {
          rankingService.getDivision(country, index);
          expect(API_v2.get).toHaveBeenCalledWith(
            `divisions/${country.toLowerCase()}/${index}`
          );
        });
      });
    });
    describe("forceDivision", () => {
      test("generate a POST request", () => {
        rankingService.forceDivision(
          "whatever",
          undefined,
          undefined,
          undefined
        );
        expect(API_v2.post).toHaveBeenCalledTimes(1);
      });
      test("call ranking url", () => {
        rankingService.forceDivision(
          "whatever",
          undefined,
          undefined,
          undefined
        );
        expect(API_v2.post).toHaveBeenCalledWith(
          "ranking",
          expect.any(Object),
          expect.any(Object)
        );
      });
      test("call #buildAuthorizationHeader from authService", () => {
        rankingService.forceDivision(
          "whatever",
          undefined,
          undefined,
          undefined
        );
        expect(authService.buildAuthorizationHeader).toHaveBeenCalledTimes(1);
      });
      test("use given arguments to generate request body", () => {
        const argument = {
          country: "Sweden",
          level: 4,
          firstLeagueId: 560,
          leagueSize: 128
        };
        rankingService.forceDivision(
          argument.country,
          argument.level,
          argument.firstLeagueId,
          argument.leagueSize
        );
        expect(API_v2.post).toHaveBeenCalledWith("ranking", argument, {
          headers: { Authorization: expect.any(String) }
        });
      });
    });
    describe("getTask", () => {
      test("generate a GET request", () => {
        rankingService.getTask("1234");
        expect(API_v2.get).toHaveBeenCalledTimes(1);
      });
      test("call #buildAuthorizationHeader from authService", () => {
        rankingService.getTask("1234");
        expect(authService.buildAuthorizationHeader).toHaveBeenCalledTimes(1);
      });
      test("use given arguments to generate GET request", () => {
        rankingService.getTask("1234");
        expect(API_v2.get).toHaveBeenCalledWith("tasks/1234", {
          headers: { Authorization: expect.any(String) }
        });
        ["ocde-defa-dayu-1234", "OPABIEYGFE", 1234].every(taskId => {
          rankingService.getTask(taskId);
          expect(API_v2.get).toHaveBeenCalledWith(`tasks/${taskId}`, {
            headers: { Authorization: expect.any(String) }
          });
        });
      });
    });
    describe.skip("getTaskEvent", () => {
      test("return an EventSource", () => {
        const res = rankingService.getTaskEvent("ocde-defa-dayu-1234");
        expect(res).toBe(expect.any(EventSource));
      });
      test("EventSource is targeting stream task api", () => {
        const res = rankingService.getTaskEvent("ocde-defa-dayu-1234");
        expect(res.url).toBe(`${STREAM_API_V2_URL}/tasks/ocde-defa-dayu-1234`);
      });
    });
    afterEach(() => {
      expect(API_v1.get).toHaveBeenCalledTimes(0);
      expect(API_v1.post).toHaveBeenCalledTimes(0);
    });
  });
});
