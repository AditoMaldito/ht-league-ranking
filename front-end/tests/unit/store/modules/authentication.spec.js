import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import authentication from "@/store/modules/authentication";

const localVue = createLocalVue();
localVue.use(Vuex);
let store;

describe("authentication store module", () => {
  beforeEach(() => {
    store = new Vuex.Store({ ...authentication });
  });
  test("Initial setup", () => {
    expect(store.state.serverAuthenticated).toBeFalsy();
  });
  describe("mutations", () => {
    describe("setAuthenticated", () => {
      test("set to true", () => {
        store.commit("setAuthenticated", true);
        expect(store.state.serverAuthenticated).toBeTruthy();
      });
      test("set to false", () => {
        store.commit("setAuthenticated", false);
        expect(store.state.serverAuthenticated).toBeFalsy();
      });
      test("set to undefined", () => {
        expect(() => {
          return store.commit("setAuthenticated", undefined);
        }).toThrowError(TypeError);
      });
    });
    describe("setToken", () => {
      test("set to given value", () => {
        store.commit("setToken", undefined);
        expect(store.state.token).toBeUndefined();
        store.commit("setToken", "qwertyuiop123456789");
        expect(store.state.token).toBe("qwertyuiop123456789");
      });
    });
    describe("setProfile", () => {
      test("set to undefined", () => {
        store.commit("setProfile", undefined);
        expect(store.state.profile).toBeUndefined();
      });
      test("set to an object", () => {
        const profile = { name: "test profile", test: true, age: 123 };
        store.commit("setProfile", profile);
        expect(store.state.profile).toMatchObject(profile);
      });
    });
  });
  describe("actions", () => {
    const commit = jest.fn();
    beforeEach(() => {
      commit.mockClear();
    });
    describe("set", () => {
      test("commit 'setAuthenticated'", () => {
        authentication.actions.set({ commit }, true);
        expect(commit).toHaveBeenCalledWith("setAuthenticated", true);
      });
    });
    describe("updateToken", () => {
      test("commit 'setToken'", () => {
        const token = "qazwsxedcrfvtgbyhnujm";
        authentication.actions.updateToken({ commit }, token);
        expect(commit).toHaveBeenCalledWith("setToken", token);
      });
    });
    describe("updateProfile", () => {
      const profile = { name: "Test Profile", test: true };
      test("commit 'setProfile'", () => {
        authentication.actions.updateProfile({ commit }, profile);
        expect(commit).toHaveBeenCalledWith("setProfile", profile);
      });
    });
    describe("logout", () => {
      test("commit 'setProfile'", () => {
        authentication.actions.logout({ commit });
        expect(commit).toHaveBeenCalledWith("setProfile", undefined);
      });
      test("commit 'setToken'", () => {
        authentication.actions.logout({ commit });
        expect(commit).toHaveBeenCalledWith("setToken", undefined);
      });
    });
  });
});
