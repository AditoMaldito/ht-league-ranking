import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import division from "@/store/modules/division";

const localVue = createLocalVue();
localVue.use(Vuex);
let store;

describe("division store module", () => {
  beforeEach(() => {
    store = new Vuex.Store({ ...division });
  });
  test("Initial setup", () => {
    expect(store.state.country).toBeUndefined();
    expect(store.state.countryId).toBeUndefined();
    expect(store.state.level).toBeUndefined();
    expect(store.state.firstLeagueId).toBeUndefined();
    expect(store.state.leagueSize).toBeUndefined();
  });
  describe("mutations", () => {
    describe("setCountry", () => {
      test("set to 'France'", () => {
        store.commit("setCountry", "France");
        expect(store.state.country).toBe("France");
      });
      test("set to not string value", () => {
        expect(() => {
          return store.commit("setCountry", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setCountry", 1234);
        }).toThrowError(TypeError);
      });
    });
    describe("setCountryId", () => {
      test("set to a number", () => {
        store.commit("setCountryId", 1234);
        expect(store.state.countryId).toBe(1234);
        store.commit("setCountryId", +89);
        expect(store.state.countryId).toBe(89);
      });
      test("set to not number value", () => {
        expect(() => {
          return store.commit("setCountryId", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setCountryId", "hello");
        }).toThrowError(TypeError);
      });
    });
    describe("setLevel", () => {
      test("set to a number", () => {
        store.commit("setLevel", 1234);
        expect(store.state.level).toBe(1234);
      });
      test("set to not number value", () => {
        expect(() => {
          return store.commit("setLevel", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setLevel", "hello");
        }).toThrowError(TypeError);
      });
    });
    describe("setFirstLeagueId", () => {
      test("set to a number", () => {
        store.commit("setFirstLeagueId", 1234);
        expect(store.state.firstLeagueId).toBe(1234);
      });
      test("set to not number value", () => {
        expect(() => {
          return store.commit("setFirstLeagueId", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setFirstLeagueId", "hello");
        }).toThrowError(TypeError);
      });
    });
    describe("setLeagueSize", () => {
      test("set to a number", () => {
        store.commit("setLeagueSize", 1234);
        expect(store.state.leagueSize).toBe(1234);
      });
      test("set to not number value", () => {
        expect(() => {
          return store.commit("setLeagueSize", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setLeagueSize", "hello");
        }).toThrowError(TypeError);
      });
    });
  });
  describe("actions", () => {
    const commit = jest.fn();
    beforeEach(() => {
      commit.mockClear();
    });
    describe("set", () => {
      test("use commit to needed state values", () => {
        const level = 3;
        const firstLeagueId = 567;
        const leagueSize = 16;
        const country = "Whatever";
        const countryId = 99999;
        division.actions.setDivision(
          { commit },
          { level, firstLeagueId, leagueSize, country, countryId }
        );
        expect(commit).toHaveBeenCalledWith("setCountry", country);
        expect(commit).toHaveBeenCalledWith("setCountryId", countryId);
        expect(commit).toHaveBeenCalledWith("setLevel", level);
        expect(commit).toHaveBeenCalledWith("setFirstLeagueId", firstLeagueId);
        expect(commit).toHaveBeenCalledWith("setLeagueSize", leagueSize);
      });
    });
  });
});
