import Vue from "vue";
import VueI18n from "vue-i18n";

Vue.use(VueI18n);

const DEFAULT_LOCALE = "en-US";

const locale = window.navigator.userLanguage || window.navigator.language;

export const availableLocales = [
  { code: DEFAULT_LOCALE, name: "English US" },
  { code: "fr-FR", name: "Français France" },
  { code: "en", name: "English" },
  { code: "fr", name: "Français" }
];

const messages = {};
const dateTimeFormats = {};

const defaultDateTimeFormat = {
  short: {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    hour: "numeric",
    minute: "numeric"
  },
  long: {
    year: "numeric",
    month: "long",
    day: "numeric",
    weekday: "long",
    hour: "numeric",
    minute: "numeric"
  }
};

availableLocales.forEach(locale => {
  const data = require(`./${locale.code}.json`);
  messages[locale.code] = data;
  dateTimeFormats[locale.code] = locale.dateTimeFormat ?? defaultDateTimeFormat;
});

export const i18n = new VueI18n({
  locale,
  messages,
  fallbackLocale: DEFAULT_LOCALE,
  availableLocales,
  dateTimeFormats
});
