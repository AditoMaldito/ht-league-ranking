import Vue from "vue";
import Buefy from "buefy";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import { i18n } from "./i18n/setup";

Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App),
  i18n
}).$mount("#app");
