export const authStatus = jest.fn(() => {
  return Promise.resolve({
    data: { authenticated: true, message: "Server already authenticated" }
  });
});

export const login = jest.fn(() => {
  return Promise.resolve({
    data: {
      authenticated: false,
      url: "https://example/com",
      message: "Please authenticate this service at https://example.com"
    },
    status: 200
  });
});

export const logout = jest.fn(() => {
  return Promise.resolve({ data: {}, status: 200 });
});

export const fetchProfile = jest.fn(() => {
  return Promise.resolve({
    data: {
      name: "Whatever"
    },
    status: 200
  });
});

export const findTokenInCookies = jest.fn(() => {
  return "MySecretToken";
});

export const storeToken = jest.fn();

export const retrieveToken = jest.fn(() => {
  return "MySecretToken";
});

export const removeToken = jest.fn();

export const buildAuthorizationHeader = jest.fn(() => {
  return { Authorization: "Bearer MySecretToken" };
});

export default {
  authStatus,
  login,
  logout,
  fetchProfile,
  findTokenInCookies,
  storeToken,
  retrieveToken,
  removeToken,
  buildAuthorizationHeader
};
