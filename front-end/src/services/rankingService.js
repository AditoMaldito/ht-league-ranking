import { API_v1, API_v2, STREAM_API_V2_URL } from "./httpService";
import { buildAuthorizationHeader } from "./authService";

const getOne = leagueId => {
  return API_v1.get(`ranking/league/${leagueId}`);
};

const getDivision = (country, level) => {
  // country = country.toLowerCase();
  const headers = buildAuthorizationHeader();
  return API_v2.get(`divisions/${country}/${level}`, { headers });
};

const forceDivision = (country, level, firstLeagueId, leagueSize) => {
  // country = country.toLowerCase();
  const headers = buildAuthorizationHeader();
  return API_v2.post(
    "ranking",
    {
      country,
      level,
      firstLeagueId,
      leagueSize
    },
    { headers }
  );
};

const getTask = taskId => {
  const headers = buildAuthorizationHeader();
  return API_v2.get(`tasks/${taskId}`, { headers });
};

const getTaskEvent = taskId => {
  const url = `${STREAM_API_V2_URL}/tasks/${taskId}`;
  return new EventSource(url);
};

const download = leagues => {
  return API_v1.post("export/ranking", { leagues: leagues });
};

export default {
  getOne,
  getDivision,
  forceDivision,
  getTask,
  getTaskEvent,
  download
};
