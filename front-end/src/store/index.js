import Vue from "vue";
import Vuex from "vuex";
import ranking from "./modules/ranking";
import authentication from "./modules/authentication";
import division from "./modules/division";
import task from "./modules/task";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    ranking,
    authentication,
    division,
    task
  }
});
