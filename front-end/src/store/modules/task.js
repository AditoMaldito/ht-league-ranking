import helper from "./helper";

const state = () => ({
  taskId: undefined,
  taskStatus: "done",
  country: undefined,
  level: undefined,
  startDate: undefined,
  ratio: undefined
});

const actions = {
  setTask({ commit }, { task_id, task_status, country, level, start_date }) {
    commit("setLevel", level);
    commit("setTaskId", task_id);
    commit("setTaskStatus", task_status);
    commit("setCountry", country);
    commit("setStartDate", start_date);
    commit("resetRatio");
  },
  updateRunningTask({ commit }, { status, ratio }) {
    commit("setTaskStatus", status);
    commit("setRatio", ratio);
  },
  resetTask({ commit }) {
    commit("resetLevel");
    commit("resetTaskId");
    commit("resetTaskStatus");
    commit("resetCountry");
    commit("resetStartDate");
    commit("resetRatio");
  }
};

const getters = {
  complete: state => {
    return Boolean(
      ["done", "failed", "canceled"].indexOf(state.taskStatus) + 1
    );
  },
  done: state => {
    return state.taskStatus === "done";
  }
};

const mutations = {
  setCountry(state, country) {
    helper.checkFunctionArgumentType("setCountry", country, "string");
    state.country = country;
  },
  resetCountry(state) {
    state.country = undefined;
  },
  setTaskId(state, taskId) {
    helper.checkFunctionArgumentType("setTaskId", taskId, "string");
    state.taskId = taskId;
  },
  resetTaskId(state) {
    state.taskId = undefined;
  },
  setLevel(state, level) {
    helper.checkFunctionArgumentType("setLevel", level, "number");
    state.level = level;
  },
  resetLevel(state) {
    state.level = undefined;
  },
  setTaskStatus(state, taskStatus) {
    helper.checkFunctionArgumentType("setTaskStatus", taskStatus, "string");
    state.taskStatus = taskStatus;
  },
  resetTaskStatus(state) {
    state.taskStatus = "done";
  },
  setStartDate(state, startDate) {
    helper.checkFunctionArgumentType("setStartDate", startDate, "string");
    state.startDate = startDate;
  },
  resetStartDate(state) {
    state.startDate = undefined;
  },
  setRatio(state, ratio) {
    helper.checkFunctionArgumentType("setRatio", ratio, "number");
    state.ratio = ratio;
  },
  resetRatio(state) {
    state.ratio = undefined;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
