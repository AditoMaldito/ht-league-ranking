import helper from "./helper";

const state = () => ({
  serverAuthenticated: false,
  token: undefined,
  profile: undefined
});

const actions = {
  set({ commit }, authStatus) {
    commit("setAuthenticated", authStatus);
  },
  updateToken({ commit }, token) {
    helper.checkFunctionArgumentType("setToken", token, "string");
    commit("setToken", token);
  },
  updateProfile({ commit }, profile) {
    helper.checkFunctionArgumentType("setToken", profile, "object");
    commit("setProfile", profile);
  },
  logout({ commit }) {
    commit("setToken", undefined);
    commit("setProfile", undefined);
  }
};

const getters = {
  userAuthenticated(state) {
    return !!state.token;
  }
};

const mutations = {
  setAuthenticated(state, authStatus) {
    helper.checkFunctionArgumentType("setAuthenticated", authStatus, "boolean");
    state.serverAuthenticated = authStatus;
  },
  setToken(state, token) {
    state.token = token;
  },
  setProfile(state, profile) {
    state.profile = profile;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
