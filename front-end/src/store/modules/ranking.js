import helper from "./helper";

const state = () => ({
  leagues: [],
  last_update: undefined,
  country: undefined,
  level: undefined,
  loading: false
});

const actions = {
  set({ commit }, { leagues, last_update, country, level }) {
    helper.checkFunctionArgumentType("setLeagues", leagues, "object");
    commit("setLeagues", leagues);
    helper.checkFunctionArgumentType("setUpdate", last_update, "string");
    commit("setUpdate", last_update);
    helper.checkFunctionArgumentType("setCountry", country, "string");
    commit("setCountry", country);
    helper.checkFunctionArgumentType("setLevel", level, "number");
    commit("setLevel", level);
  },
  add({ commit }, league) {
    commit("addLeague", league);
  },
  toggleLoading({ commit }) {
    commit("toggleLoading");
  },
  reset({ commit }) {
    commit("setLeagues", []);
    commit("setUpdate", undefined);
    commit("setCountry", undefined);
    commit("setLevel", undefined);
  }
};

const mutations = {
  setLeagues(state, leagues) {
    state.leagues = leagues;
  },
  setUpdate(state, update) {
    state.last_update = update;
  },
  setCountry(state, country) {
    state.country = country;
  },
  setLevel(state, level) {
    state.level = level;
  },
  toggleLoading(state) {
    state.loading = !state.loading;
  },
  addLeague(state, league) {
    helper.checkFunctionArgumentType("addLeague", league, "object");
    state.leagues.push(league);
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
