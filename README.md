# HT League Ranking

HT League ranking is a tool to help player from [hattrick](https://hattrick.org), a football team managment game.

It's a league finder helper based on [power rating metric](https://devblog.hattrick.org/2020/04/how-to-understand-the-hattrick-power-rating/) where you can get a ranking for all leagues for a specific country and a specific division.

Here are some usecase for HT League Ranking:

- Find a league to play with several friends
- Find a challenging league according to your team level
- Find a league that fit your team plan (rise, uphold, etc...)

You can [use HT League Ranking](https://ht-league-ranking.com) on a desktop or mobile. You can also install HT League Ranking as mobile application when visiting the [web site](https://ht-league-rankig.com) thanks to [PWA](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps) (better experience using google chrome on Android).

## Use your own instance

HT League Ranking use [docker images](https://gitlab.com/LeoMouyna/ht-league-ranking/container_registry) that can be used to deploy your own instance. You will need to place a nginx configuration inside nginx shared volume ([an example](./front-end/nginx.example.prod.conf) is available).

## Feedbacks

If you find something that doesn't work as exepected please report it:

- By [submitting an issue](https://gitlab.com/LeoMouyna/ht-league-ranking/-/issues/new?) (you have to login into [gitlab](https://gitlab.com) first).
- By sending email to our [help desk](mailto:incoming+leomouyna-ht-league-ranking-18798057-issue-@incoming.gitlab.com)
- By considering [contributing to this project](./CONTRIBUTING.md) (We need testers and developpers)

## Technical Architecture

For those who are interested about HT League Ranking architecture we use:

- [Vue](https://vuejs.org) as frontend framework
- [Buefy](https://buefy.org) as component library
- [Nginx](https://nginx.org) as reverse proxy
- [Flask](https://flask.palletsprojects.com) as backend framework
- [Celery](https://docs.celeryproject.org) as task queue
- [Redis](https://redis.io/) as message broker
- [Alembic](https://alembic.sqlalchemy.org) as database migration tool
- [Postgresql](https://postgresql.org) as database
