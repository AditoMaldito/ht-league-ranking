"""Create divisions table

Revision ID: 61a86c447482
Revises: c019eab85ee9
Create Date: 2020-12-12 19:21:12.472928

"""
from alembic import op
import sqlalchemy as sa
from datetime import datetime


# revision identifiers, used by Alembic.
revision = '61a86c447482'
down_revision = 'c019eab85ee9'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'divisions',
        sa.Column('country', sa.String, primary_key=True),
        sa.Column('level', sa.Integer, primary_key=True),
        sa.Column('last_update', sa.DateTime,
                  nullable=False, default=datetime.utcnow),
        sa.Column('leagues', sa.JSON(), nullable=False),
    )


def downgrade():
    op.drop_table('divisions')
