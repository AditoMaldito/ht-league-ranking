"""create ranking_tasks table

Revision ID: f2487bc0969f
Revises: 61a86c447482
Create Date: 2021-01-02 13:00:51.458040

"""
from alembic import op
import sqlalchemy as sa
from datetime import datetime
from enum import Enum


# revision identifiers, used by Alembic.
revision = 'f2487bc0969f'
down_revision = '61a86c447482'
branch_labels = None
depends_on = None


class TaskStatusEnum(Enum):
    pending = 'pending'
    waiting = 'waiting'
    done = 'done'
    canceled = 'canceled'
    failed = 'failed'
    running = 'running'


def upgrade():
    op.create_table(
        'ranking_tasks',
        sa.Column('task_id', sa.Integer, primary_key=True),
        sa.Column('task_status', sa.Enum(TaskStatusEnum), nullable=False),
        sa.Column('country', sa.String(), nullable=False),
        sa.Column('level', sa.Integer, nullable=False),
        sa.Column('start_date', sa.DateTime,
                  nullable=False, default=datetime.utcnow),
    )


def downgrade():
    op.drop_table('ranking_tasks')
