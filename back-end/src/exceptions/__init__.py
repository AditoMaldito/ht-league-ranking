from werkzeug.exceptions import HTTPException
from werkzeug.sansio.response import Response
from typing import Optional
from json import dumps


class APIHTTPException(HTTPException):
    def __init__(
        self,
        status_code: int,
        message_code: str = "errors.somethingWentWrong",
        description: Optional[str] = None,
        response: Optional[Response] = None,
    ) -> None:
        super().__init__(description=description, response=response)
        self.code = status_code
        self.message_code = message_code

    def get_json_response(self) -> Response:
        response = self.get_response()
        data = {"code": self.message_code, "name": self.name}
        if self.description:
            data["message"] = self.description
        response.data = dumps(data)
        response.content_type = "application/json"
        return response

    def __str__(self) -> str:
        return f"{self.code} {self.name} - {self.message_code}"

    def __repr__(self) -> str:
        return f"<{type(self).__name__} '{self.code} {self.name} - {self.message_code}'>"
