from flask import Flask
from flask.logging import default_handler
from flask_cors import CORS
from flask_restful import Api
from src.api_v1 import api as api_v1
import logging
from src.api_v2 import stream_api as api_v2_stream
from src.api_v2.country.resources import CountryListResource, CountryResource
from src.api_v2.division.resources import DivisionListResource, DivisionResource
from src.api_v2.ranking.resources import RankingResource
from src.api_v2.task.resources import TaskListResource, TaskResource
from src.api_v2.auth import auth as api_v2_auth
from src.exceptions import APIHTTPException


app = Flask(__name__)
app.config["OPENAPI_VERSION"] = "3.0.2"
app.register_blueprint(api_v1)
app.register_blueprint(api_v2_stream)
app.register_blueprint(api_v2_auth)

CORS(app, ressources={r"/api/*": {"origins": "localhost*"}})


@app.errorhandler(APIHTTPException)
def handle_api_http_errors(e: APIHTTPException):
    return e.get_json_response()


api_v2 = Api(app, prefix="/api/v2")

api_v2.add_resource(CountryListResource, "/countries")
api_v2.add_resource(CountryResource, "/countries/<int:ht_league_id>")
api_v2.add_resource(DivisionListResource, "/divisions")
api_v2.add_resource(DivisionResource, "/divisions/<country>/<int:level>")
api_v2.add_resource(RankingResource, "/ranking")
api_v2.add_resource(TaskListResource, "/tasks")
api_v2.add_resource(TaskResource, "/tasks/<task_id>")


if __name__ == "__main__":
    for logger in (
        app.logger,
        logging.getLogger("long_task"),
        logging.getLogger("api_v2"),
    ):
        logger.addHandler(default_handler)
        logger.setLevel(logging.DEBUG)
    app.run()

if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")

    if len(gunicorn_logger.handlers):
        app.logger.handlers = gunicorn_logger.handlers
    if gunicorn_logger.level:
        app.logger.setLevel(gunicorn_logger.level)
