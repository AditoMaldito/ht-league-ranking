from functools import wraps
from src.api_v2.auth.token import validate_token, retrieve_token
from flask import request
from os import environ
from sqlalchemy import create_engine
from sqlalchemy.orm import Session


def catch_missing_params(func):
    def wrapper(*arg, **kwargs):
        try:
            return func(*arg, **kwargs)
        except KeyError as e:
            message = f"{e.args[0]} is mandatory"
            return {"message": message}, 400

    return wrapper


def database_connection(func):
    def wrapper(*arg, **kwrags):
        engine = create_engine(environ["DATABASE_URL"])
        with Session(engine) as session:
            res = func(*arg, **kwrags, session=session)
        engine.dispose()
        return res

    return wrapper


def _unauthorized_response(message: str = "Unauthorized", code: str = "unauthorized"):
    return {"message": message, "code": code}, 401


def require_token(func):
    @wraps(func)
    def wrapper(*arg, **kwrags):
        token = request.headers.get("Authorization")
        if not token:
            return _unauthorized_response("authorization header is not set", "auth.missingHeader")
        try:
            token = token.split("Bearer ")[1]
        except IndexError:
            return _unauthorized_response(
                "authorization header is not set with bearer token", "auth.nonBearerHeader"
            )
        if not validate_token(token):
            return _unauthorized_response("token is invalid", "auth.invalidToken")
        payload = retrieve_token(request)
        return func(*arg, **kwrags, token=payload)

    return wrapper
