from datetime import datetime
import logging
import os
from src.api_v2.constants import REDIS_URL

from celery import Task
from redis import Redis, ConnectionError
from json import dumps
from src.api_v2.task.models import EnumEncoder, RankingTaskModel, TaskStatusEnum
from src.api_v2.division.models import DivisionModel
from src.decorator import database_connection
from sqlalchemy.orm import Session


class RankingTask(Task):
    @database_connection
    def on_failure(self, exc, task_id, args, kwargs, einfo, session: Session):
        """
        Run when a Task fail
        Update database entry with failed as status
        """
        logging.error(einfo)
        message = {
            "status": TaskStatusEnum.failed,
            "done": True,
            "ratio": 0,
            "description": str(exc),
        }
        if hasattr(exc, "message_code"):
            message["description"] = exc.message_code
        redis_message = dumps(message, cls=EnumEncoder)
        try:
            task = session.query(RankingTaskModel).get(task_id)
            task.task_status = TaskStatusEnum.failed
            updated_task = session.merge(task)
            session.add(updated_task)
            session.commit()
            redis = Redis.from_url(os.environ[REDIS_URL])
            redis.publish(task_id, redis_message)
            redis.close()
        except ConnectionError as e:
            logging.error(f"Can't publish {redis_message} cause:\n{e}")
        except Exception as e:
            session.rollback()
            kwargs["exception"] = e
            super().on_failure(exc, task_id, args, kwargs, einfo)

    @database_connection
    def on_success(self, retval, task_id, args, kwargs, session: Session):
        """
        Run when a task is done.
        Update database entries:
            - Ranking Task status to done
            - Divisions leagues to retval arguments
        """
        redis_message = ""
        try:
            logging.info("success for %s", task_id)
            task = session.query(RankingTaskModel).get(task_id)
            task.task_status = TaskStatusEnum.done
            updated_task = session.merge(task)
            session.add(updated_task)

            division = session.query(DivisionModel).get([updated_task.country, updated_task.level])

            if not division:
                logging.warning(
                    "No division found for %s on level %d", updated_task.country, updated_task.level
                )
                logging.info("Create a new division")
                updated_division = DivisionModel(
                    country=updated_task.country, level=updated_task.level, leagues=retval
                )

            else:
                division.leagues = retval
                division.last_update = datetime.utcnow()
                updated_division = session.merge(division)

            session.add(updated_division)
            session.commit()
            redis_message = dumps(
                {"status": TaskStatusEnum.done, "done": True, "ratio": 100}, cls=EnumEncoder
            )

        except Exception as e:
            session.rollback()
            logging.error(e)
            task = session.query(RankingTaskModel).get(task_id)
            task.task_status = TaskStatusEnum.failed
            updated_task = session.merge(task)
            session.add(updated_task)
            session.commit()
            redis_message = dumps(
                {"status": TaskStatusEnum.failed, "done": True, "ratio": 0}, cls=EnumEncoder
            )

        finally:
            try:
                redis = Redis.from_url(os.environ[REDIS_URL])
                redis.publish(task_id, redis_message)
                redis.close()
            except ConnectionError as e:
                logging.error(f"Can't publish {redis_message} cause:\n{e}")

            except Exception as e:
                logging.error(f"Can't publish {redis_message} cause:\n{e}")
