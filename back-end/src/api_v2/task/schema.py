from flask_marshmallow import Marshmallow
from src.api_v2.task.models import TaskStatusEnum
from marshmallow_enum import EnumField

ma = Marshmallow()


class RankingTaskSchema(ma.Schema):
    task_status = EnumField(TaskStatusEnum)

    class Meta:
        fields = ("start_date", "task_id", "country", "level", "task_status")


ranking_task_schema = RankingTaskSchema()
ranking_tasks_schema = RankingTaskSchema(many=True)
