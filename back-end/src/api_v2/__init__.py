"""
API v2 module
"""

from src.api_v2.task.helper import stream_task_status
from src.api_v2.auth.ht_oauth import HTOAuth
from src.logger import set_logger
from os import environ
from flask import Response, Blueprint
from src.api_v2.constants import HT_ACCESS_TOKEN_SECRET, HT_CONSUMER_KEY
from src.api_v2.constants import HT_CONSUMER_SECRET, HT_ACCESS_TOKEN_KEY

_consumer_key = environ[HT_CONSUMER_KEY]
_consumer_secret = environ[HT_CONSUMER_SECRET]
_access_token_key = environ[HT_ACCESS_TOKEN_KEY]
_access_token_secret = environ[HT_ACCESS_TOKEN_SECRET]

session = HTOAuth(
    consumer_key=_consumer_key,
    consumer_secret=_consumer_secret,
    access_token_key=_access_token_key,
    access_token_secret=_access_token_secret,
).session


stream_api = Blueprint("api_v2", __name__, url_prefix="/api/v2/stream")

logger = set_logger("api_v2")


@stream_api.route("/tasks/<task_id>")
def stream_task(task_id):
    logger.info(f"Start stream task {task_id}")
    return Response(stream_task_status(task_id), mimetype="text/event-stream")
