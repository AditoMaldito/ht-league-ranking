from sqlalchemy.schema import Column
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.types import String, Integer
from sqlalchemy.orm import declarative_base


class CountryModel(declarative_base()):
    """
    Country Data Model
    """

    __tablename__ = "countries"

    ht_league_id = Column(Integer, primary_key=True)
    name = Column(String())
    leagues = Column(JSON)

    def __init__(self, ht_league_id, name, leagues):
        self.ht_league_id = ht_league_id
        self.name = name
        self.leagues = leagues

    def __repr__(self):
        return f"<Country: {self.name}>"
