from flask import request, abort
from flask_restful import Resource
from src.api_v2.country.models import CountryModel
from src.api_v2.country.schemas import countries_schema, country_schema
from src.api_v2 import logger
from sqlalchemy import func
from src.decorator import catch_missing_params, database_connection
from sqlalchemy.orm import Session


class CountryListResource(Resource):
    @database_connection
    def get(self, session: Session):
        args = request.args
        name = args.get("name", "").lower()
        countries = session.query(CountryModel).filter(func.lower(CountryModel.name).contains(name))
        logger.debug(f'Found {countries.count()} countrie(s) matching name "{name}"')
        return countries_schema.dump(countries), 200

    @catch_missing_params
    @database_connection
    def post(self, session: Session):
        new_country = CountryModel(
            ht_league_id=request.json["ht_league_id"],
            name=request.json["name"],
            leagues=request.json["leagues"],
        )
        logger.debug(f"Add country {new_country}")
        session.add(new_country)
        session.commit()
        return country_schema.dump(new_country), 201


class CountryResource(Resource):
    @database_connection
    def get(self, ht_league_id: int, session: Session):
        description = f"No country found with ht_league_id: {ht_league_id}"
        country = session.get(CountryModel, ht_league_id)
        logger.debug(f"Looking for country {ht_league_id}")
        if country is None:
            abort(404, description=description)
        return country_schema.dump(country), 200

    @catch_missing_params
    @database_connection
    def patch(self, ht_league_id: int, session: Session):
        description = f"No country found with ht_league_id: {ht_league_id}"
        country = session.get(CountryModel, ht_league_id)
        logger.debug(f"Looking for country {ht_league_id}")
        if country is None:
            abort(404, description=description)

        if "name" in request.json:
            logger.debug(f"Update country name to {request.json['name']}")
            country.name = request.json["name"]
        if "leagues" in request.json:
            logger.debug(f"Update country leagues to {request.json['leagues']}")
            country.leagues = request.json["leagues"]

        local_country = session.merge(country)
        session.add(local_country)
        session.commit()
        return country_schema.dump(local_country), 200
