from typing import Dict
from flask import request
from flask_restful import Resource
from src.api_v2 import logger
from src.api_v2.task.models import RankingTaskModel, TaskStatusEnum
from src.api_v2.task.schema import ranking_task_schema
from src.long_task.ranking.tasks import compute_ranking
from src.decorator import catch_missing_params, database_connection, require_token
from sqlalchemy.orm import Session

launched_status = [TaskStatusEnum.running, TaskStatusEnum.pending, TaskStatusEnum.waiting]


class RankingResource(Resource):
    @require_token
    @catch_missing_params
    @database_connection
    def post(self, session: Session, *args, **kwargs):
        """
        1. Launch a league ranking computation if needed
            (i.e. if there is not an other task running for
            the same country, level tuple)
        2. Save new Ranking Task in database if needed
            (i.e. same condiontion than previous)
        3. Return Ranking Task data
        """
        country = request.json["country"]
        level = request.json["level"]
        first_league_id = request.json["firstLeagueId"]
        league_size = request.json["leagueSize"]

        return get_task(
            country=country,
            level=level,
            first_league_id=first_league_id,
            league_size=league_size,
            session=session,
        )


def get_task(
    country: str, level: int, first_league_id: int, league_size: int, session: Session
) -> Dict:
    logger.debug(f"Looking for task on {country} division {level}")
    current_task = session.query(RankingTaskModel).filter(
        RankingTaskModel.country == country,
        RankingTaskModel.level == level,
        RankingTaskModel.task_status.in_(launched_status),
    )
    if current_task.first():
        logger.info(f"A task is already running for {country} division {level}")
        return ranking_task_schema.dump(current_task.first()), 200

    task_instance = compute_ranking.delay(first_league_id, league_size)
    current_task = RankingTaskModel(
        task_id=task_instance.id, task_status=TaskStatusEnum.pending, country=country, level=level
    )
    logger.info(f"Add task {task_instance.id} for {country} division {level}")
    session.add(current_task)
    session.commit()
    return ranking_task_schema.dump(current_task), 201
