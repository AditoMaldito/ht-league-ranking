from flask import request, abort
from flask_restful import Resource
from src.api_v2 import logger
from src.api_v2.division.models import DivisionModel
from src.api_v2.division.schemas import divisions_schema, division_schema
from datetime import datetime
from src.decorator import catch_missing_params, database_connection
from sqlalchemy.orm import Session


class DivisionListResource(Resource):
    @database_connection
    def get(self, session: Session):
        divisions = session.query(DivisionModel).all()
        logger.debug(f"Found {divisions.count()} division(s)")
        return divisions_schema.dump(divisions), 200

    @catch_missing_params
    @database_connection
    def post(self, session: Session):
        new_division = DivisionModel(
            country=request.json["country"],
            level=request.json["level"],
            leagues=request.json["leagues"],
        )
        logger.debug(f"Add division {new_division}")
        session.add(new_division)
        session.commit()
        return division_schema.dump(new_division), 201


class DivisionResource(Resource):
    @database_connection
    def get(self, country: str, level: int, session: Session):
        description = f"No division found for {country} on level {level}"
        division = session.get(DivisionModel, {"country": country, "level": level})
        logger.debug(f"Looking for division {level} on {country}")
        if division is None:
            abort(404, description=description)
        return division_schema.dump(division), 200

    @catch_missing_params
    @database_connection
    def patch(self, country: str, level: int, session: Session):
        description = f"No division found for {country} on level {level}"
        division = session.get(DivisionModel, {"country": country, "level": level})
        logger.debug(f"Looking for division {level} on {country}")
        if division is None:
            abort(404, description=description)
        division.leagues = request.json["leagues"]
        division.last_update = datetime.utcnow()
        local_division = session.merge(division)
        logger.debug(f"Update to {local_division}")
        session.add(local_division)
        session.commit()
        return division_schema.dump(local_division), 200
