from src.api_v2.auth.ht_oauth import build_client_session
from src.exceptions import APIHTTPException
from typing import Optional
from xml.etree.ElementTree import Element, fromstring
from src.logger import set_logger
from json import dumps

logger = set_logger("api_v2.auth.models")


class Profile:
    def __init__(self, name: str) -> None:
        self.name = name

    @classmethod
    def from_xml(cls, xml: Element):
        name = xml.find("Loginname").text
        return Profile(name=name)


def fetch_profile(access_key: str, user_id: Optional[str] = None):
    file = "managercompendium"
    api_version = "1.5"
    session = build_client_session(access_key)
    payload = {"file": file, "version": api_version}
    if user_id:
        payload["user_id"] = user_id

    r = session.get("", params=payload)
    if r.status_code != 200:
        logger.warning(
            f"Pb when fetching user {user_id or access_key}. Receive a {r.status_code} response"
        )

        if r.status_code == 401:
            logger.debug(r.text)
            logger.debug(dumps(r.__dict__))
            raise APIHTTPException(r.status_code, "errors.hattrickRejections", r.text)

        raise APIHTTPException(status_code=r.status_code, description=r.text)

    xml = fromstring(r.text)
    profile_xml = xml.find("Manager")

    return Profile.from_xml(profile_xml)
