from os import environ
from typing import Dict
from flask.wrappers import Request
from src.api_v2.constants import JWT_TOKEN_SECRET
from jwt import decode, exceptions
from datetime import datetime


def validate_token(token: str) -> bool:
    secret = environ[JWT_TOKEN_SECRET]
    try:
        payload = decode(token, secret, algorithms="HS256")
        return int(payload.get("deprecatedAt")) > datetime.now().timestamp()
    except exceptions.InvalidTokenError:
        return False


def retrieve_token(request: Request) -> Dict:
    token_header = request.headers.get("Authorization")
    token = token_header.split("Bearer ")[1]
    secret = environ[JWT_TOKEN_SECRET]
    payload = decode(token, secret, algorithms="HS256")
    return payload
