from rauth import OAuth1Service, OAuth1Session


class HTOAuth:
    def __init__(self, consumer_key, consumer_secret, access_token_key="", access_token_secret=""):
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.access_token_key = access_token_key
        self.access_token_secret = access_token_secret

        self.service = OAuth1Service(
            name="hattrick",
            consumer_key=self.consumer_key,
            consumer_secret=self.consumer_secret,
            request_token_url="https://chpp.hattrick.org/oauth/request_token.ashx",
            access_token_url="https://chpp.hattrick.org/oauth/access_token.ashx",
            authorize_url="https://chpp.hattrick.org/oauth/authorize.aspx",
            base_url="https://chpp.hattrick.org/chppxml.ashx",
        )

    def get_request_token(self, callback_url="oob"):
        return self.service.get_request_token(params={"oauth_callback": callback_url})

    def get_url(self, request_token_key):
        return self.service.get_authorize_url(request_token_key)

    def get_access_token(self, request_token_key, request_token_secret, oauth_verifier):
        return self.service.get_access_token(
            request_token_key,
            request_token_secret,
            method="POST",
            params={"oauth_verifier": oauth_verifier},
        )

    def open_session(self):
        return OAuth1Session(
            self.consumer_key,
            self.consumer_secret,
            access_token=self.access_token_key,
            access_token_secret=self.access_token_secret,
            service=self.service,
        )
