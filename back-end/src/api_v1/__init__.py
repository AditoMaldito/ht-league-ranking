from json import loads
import logging
from os import environ
from flask import Blueprint, jsonify, Response, request, url_for, redirect
from src.api_v1.encoder import LeagueJSONEncoder
from src.api_v1.helper import stream_csv_ranks, fetch_league_data
from src.api_v1.models import League
from src.api_v1.ht_oauth import HTOAuth

api = Blueprint("api_v1", __name__, url_prefix="/api/v1")

HT_CONSUMER_KEY = "HT_CONSUMER_KEY"
HT_CONSUMER_SECRET = "HT_CONSUMER_SECRET"
HT_ACCESS_TOKEN_KEY = "HT_ACCESS_TOKEN_KEY"
HT_ACCESS_TOKEN_SECRET = "HT_ACCESS_TOKEN_SECRET"
HT_REQUEST_TOKEN_KEY = "HT_REQUEST_TOKEN_KEY"
HT_REQUEST_TOKEN_SECRET = "HT_REQUEST_TOKEN_SECRET"


def get_session():
    consumer_key = environ[HT_CONSUMER_KEY]
    consumer_secret = environ[HT_CONSUMER_SECRET]
    access_token_key = environ[HT_ACCESS_TOKEN_KEY]
    access_token_secret = environ[HT_ACCESS_TOKEN_SECRET]

    return HTOAuth(
        consumer_key=consumer_key,
        consumer_secret=consumer_secret,
        access_token_key=access_token_key,
        access_token_secret=access_token_secret,
    ).open_session()


def get_service():
    consumer_key = environ[HT_CONSUMER_KEY]
    consumer_secret = environ[HT_CONSUMER_SECRET]

    return HTOAuth(consumer_key=consumer_key, consumer_secret=consumer_secret)


@api.get("/ranking/league/<int:id>")
def league_rank(id):
    if not (environ.get(HT_ACCESS_TOKEN_KEY) and environ.get(HT_ACCESS_TOKEN_SECRET)):
        return f'Unauthenticaed service. Look at {url_for("api.auth_init")}', 401

    session = get_session()
    res = fetch_league_data(League(id), session)
    api.json_encoder = LeagueJSONEncoder
    return jsonify(res)


@api.post("/export/ranking")
def export_leagues_rank_csv():
    try:
        params = loads(request.data)
        if not params["leagues"]:
            raise ValueError("You should provide on your data `leagues`")
        leagues = params["leagues"]
        return Response(stream_csv_ranks(leagues), mimetype="application/text")
    except ConnectionResetError as e:
        logging.error(e)
        message = "Can't proceed to export du to internal error"
        return message, 500


@api.get("/auth/status")
def auth_init():
    if environ.get(HT_ACCESS_TOKEN_KEY) and environ.get(HT_ACCESS_TOKEN_SECRET):
        return jsonify({"authenticated": True, "message": "Already authenticated"})
    else:
        try:
            service = get_service()
        except KeyError as e:
            return f"Something went wrong during authentication !\n {e} key not set.", 500
        request_token_key, request_token_secret = service.get_request_token(
            url_for("api.auth_validate")
        )
        environ[HT_REQUEST_TOKEN_KEY] = request_token_key
        environ[HT_REQUEST_TOKEN_SECRET] = request_token_secret
        authorize_url = service.get_url(request_token_key)
        return jsonify(
            {
                "authenticated": False,
                "url": authorize_url,
                "message": f"Please authenticate this service at {authorize_url}",
            }
        )


@api.get("/auth/validate")
def auth_validate():
    pin = request.args.get("oauth_verifier")
    try:
        service = get_service()
        request_token_key = environ[HT_REQUEST_TOKEN_KEY]
        request_token_secret = environ[HT_REQUEST_TOKEN_SECRET]
    except KeyError as e:
        return f"Something went wrong during authentication: \n{e} key not found", 500
    access_token_key, access_token_secret = service.get_access_token(
        request_token_key=request_token_key,
        request_token_secret=request_token_secret,
        oauth_verifier=pin,
    )
    environ[HT_ACCESS_TOKEN_KEY] = access_token_key
    environ[HT_ACCESS_TOKEN_SECRET] = access_token_secret
    return redirect(url_for("index"))
