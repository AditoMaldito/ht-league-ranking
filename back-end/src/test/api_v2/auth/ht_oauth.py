from typing import Any, Union
from pytest import raises
from rauth.session import OAuth1Session
from src.api_v2.auth.ht_oauth import HTOAuth, HTOAuthClient, build_client_session
from src.api_v2.auth.errors import AuthError
from redis import Redis
from src.api_v2.constants import HT_CONSUMER_SECRET, HT_CONSUMER_KEY
from os import environ


def _assert_common_service(service):
    assert service.request_token_url == "https://chpp.hattrick.org/oauth/request_token.ashx"
    assert service.access_token_url == "https://chpp.hattrick.org/oauth/access_token.ashx"
    assert service.base_url == "https://chpp.hattrick.org/chppxml.ashx"


def test_ht_oauth_server_init():
    server = HTOAuth("test", "test")
    assert server.service.name == "server"
    assert server.service.consumer_key == "test"
    assert server.service.consumer_secret == "test"
    assert server.service.authorize_url == "https://chpp.hattrick.org/oauth/authorize.aspx"
    return _assert_common_service(server.service)


def test_ht_oauth_client_init():
    server = HTOAuthClient("test", "test")
    assert server.service.name == "client"
    assert server.service.consumer_key == "test"
    assert server.service.consumer_secret == "test"
    assert server.service.authorize_url == "https://chpp.hattrick.org/oauth/authenticate.aspx"
    return _assert_common_service(server.service)


class MockRedis:
    def __init__(self) -> None:
        self.value = {"12345": "qwertyuiop"}

    def get(self, key: str) -> Union[Any, None]:
        return self.value.get(key)

    def close(self):
        pass


def test_build_client_session_valid(monkeypatch):
    def mock_redis(*args, **kwargs):
        return MockRedis()

    monkeypatch.setattr(Redis, "from_url", mock_redis)
    consumer_key = "test"
    consumer_secret = "test_secret"
    environ[HT_CONSUMER_KEY] = consumer_key
    environ[HT_CONSUMER_SECRET] = consumer_secret

    res = build_client_session("12345")
    assert res.access_token == "12345"
    assert res.access_token_secret == "qwertyuiop"
    assert res.consumer_secret == consumer_secret
    assert res.consumer_key == consumer_key
    assert isinstance(res, OAuth1Session)


def test_build_client_session_invalid(monkeypatch):
    def mock_redis(*args, **kwargs):
        return MockRedis()

    monkeypatch.setattr(Redis, "from_url", mock_redis)
    consumer_key = "test"
    consumer_secret = "test_secret"
    environ[HT_CONSUMER_KEY] = consumer_key
    environ[HT_CONSUMER_SECRET] = consumer_secret

    with raises(AuthError):
        build_client_session("01234")
