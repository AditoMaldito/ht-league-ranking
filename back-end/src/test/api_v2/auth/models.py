from src.api_v2.auth.models import Profile
from xml.etree.ElementTree import parse
from src.test import RESOURCE_DIR
from os.path import join


def test_init_Profile_from_xml():
    tree = parse(str(join(RESOURCE_DIR, "manager.xml")))
    xml = tree.getroot()
    profile_xml = xml.find("Manager")

    profile = Profile.from_xml(profile_xml)

    assert isinstance(profile, Profile)
    assert profile.name == "potedeo"
