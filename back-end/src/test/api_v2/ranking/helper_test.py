from src.test import RESOURCE_DIR
from os.path import join
from src.api_v2.ranking.helper import Team, extract_team_xml, League, get_leagues, Stats, get_league
from src.api_v2 import session
from xml.etree.ElementTree import parse
from statistics import mean, median


def test_bot_team_from_xml():
    tree = parse(str(join(RESOURCE_DIR, "bot_team.xml")))
    xml = tree.getroot()
    team = Team.from_xml(xml)

    assert team.is_bot
    assert team.team_id == 586349
    assert team.power_rating == 545


def test_human_team_from_xml():
    tree = parse(str(join(RESOURCE_DIR, "human_team.xml")))
    xml = tree.getroot()
    team = Team.from_xml(xml)

    assert not team.is_bot
    assert team.team_id == 1164376
    assert team.power_rating == 710


def test_extract_team_xml_with_multiple():
    response_tree = parse(str(join(RESOURCE_DIR, "response_multiple_teams.xml")))
    xml_response = response_tree.getroot()
    # Should get second team
    team_xml = extract_team_xml(xml_response, 242567)
    expected_xml = xml_response[5][1]
    assert team_xml.attrib == expected_xml.attrib


class MockTeamResponse:
    @property
    def text(self):
        return open(join(RESOURCE_DIR, "league_teams.xml")).read()

    @property
    def status_code(self):
        return 200


def league_assertion(league: League):
    # Check league data
    assert league.level == 6
    assert league.bot_counter == 2
    assert league.league_id == 36937
    assert league.name == "VI.949"
    assert (
        league.url
        == "https://www.hattrick.org/goto.ashx?path=/World/Series/?LeagueLevelUnitID=36937"
    )

    # Check league stats computation
    assert league.stats.maximum == 757
    assert league.stats.human_mean == float("{:.3f}".format(mean([757, 734, 710, 746, 653, 620])))
    assert league.stats.human_mean == 703.333
    assert league.stats.human_median == median([757, 734, 710, 746, 653, 620])
    assert league.stats.mean == float(
        "{:.3f}".format(mean([757, 734, 710, 746, 653, 620, 545, 538]))
    )
    assert league.stats.mean == 662.875
    assert league.stats.median == median([757, 734, 710, 746, 653, 620, 545, 538])


def test_league_from_xml(monkeypatch):
    def mock_get_team(*args, **kwargs):
        return MockTeamResponse()

    monkeypatch.setattr(session, "get", mock_get_team)

    tree = parse(str(join(RESOURCE_DIR, "response_league.xml")))
    xml = tree.getroot()
    league = League.from_xml(xml)

    league_assertion(league)


class MockTeamMaxBotResponse:
    @property
    def text(self):
        return open(join(RESOURCE_DIR, "league_teams_max_bot.xml")).read()

    @property
    def status_code(self):
        return 200


def test_max_bot_league_from_xml(monkeypatch):
    def mock_get_team(*args, **kwargs):
        return MockTeamMaxBotResponse()

    monkeypatch.setattr(session, "get", mock_get_team)

    tree = parse(str(join(RESOURCE_DIR, "response_league.xml")))
    xml = tree.getroot()
    league = League.from_xml(xml)

    assert league.stats.maximum == 746


class MockBotTeamResponse:
    @property
    def text(self):
        return open(join(RESOURCE_DIR, "bot_league_teams.xml")).read()

    @property
    def status_code(self):
        return 200


def test_bot_league_from_xml(monkeypatch):
    def mock_get_team(*args, **kwargs):
        return MockBotTeamResponse()

    monkeypatch.setattr(session, "get", mock_get_team)

    tree = parse(str(join(RESOURCE_DIR, "response_bot_league.xml")))
    xml = tree.getroot()
    league = League.from_xml(xml)

    # Check league human metrics computation
    assert league.stats.human_mean == 0
    assert league.stats.human_median == 0
    assert league.stats.maximum == 0


class MockLeagueResponse:
    @property
    def text(self):
        return open(join(RESOURCE_DIR, "response_league.xml")).read()

    @property
    def status_code(self):
        return 200


def test_get_league(monkeypatch):
    def mock_session_get(*args, **kwargs):
        request_file = kwargs["params"]["file"]
        return MockLeagueResponse() if request_file == "leaguedetails" else MockTeamResponse()

    monkeypatch.setattr(session, "get", mock_session_get)
    league = get_league(1)

    league_assertion(league)


def test_get_leagues(monkeypatch):
    def mock_get_league(*args, **kwargs):
        stat = Stats()
        return League(1, "test", 1, 0, stat)

    monkeypatch.setattr("src.api_v2.ranking.helper.get_league", mock_get_league)

    leagues = get_leagues(1, 64, "test-channel")

    assert len(leagues) == 64
    assert leagues[0]["leagueId"] == 1
    assert leagues[0]["name"] == "test"
    assert leagues[0]["division"] == 1
    assert leagues[0]["botCount"] == 0
    assert leagues[0]["humanMean"] == 0
    assert leagues[0]["humanMedian"] == 0
    assert leagues[0]["median"] == 0
    assert leagues[0]["mean"] == 0
    assert leagues[0]["maximum"] == 0
