import logging
from flask.logging import default_handler


def set_logger(logger_name: str) -> logging.Logger:
    logger = logging.getLogger(logger_name)
    gunicorn_logger = logging.getLogger("gunicorn.error")

    if len(gunicorn_logger.handlers):
        logger.handlers = gunicorn_logger.handlers
    else:
        logger.addHandler(default_handler)
    if gunicorn_logger.level:
        logger.setLevel(gunicorn_logger.level)

    return logger
