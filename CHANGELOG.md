# Changelog

## v1.3.1

### Bug fixes

- Handle unauthorized responses. #73
- Check authentication on all application pages. #74

## v1.3.0

### Features

- Use a language selector to handle transation. #60
- Implement JWT authentication. #50 #51 #52

### Bug fixes

- Handle redis conection errors. #70 #72
- Handle connection reset by peer error during result export. #71

## v1.2.0

### Breaking changes

- Remove custom list form. #65

## v1.1.2

### Maintenance

- Set better logs to back-end. #64
- Update Flask version to v2. #67

### Bug fixes

- Handle redis closure on SSE stream generation. #63
- Too long french translation on mobile devices. #67

## v1.1.1

### Maintenance

- Fix Flask version to lower than v2

## 1.1.0

### Features

- Handle translation. #9

## 1.0.5

### Bug fixes

- Catch redis connection error. #58

## 1.0.4

### Bug fixes

- Remove bot teams to power ranking maximum computation. #57

## 1.0.3

### Themes

- Add white border to logo. #48

### Bug fixes

- Not found pages redirect to home page. #55

## 1.0.2

### Themes

- Use an other waiting gif. #53

### Features

- Generate SSE more often. #54

## 1.0.1

### Bug fixes

- Store datetime as utc. #47

## 1.0.0
